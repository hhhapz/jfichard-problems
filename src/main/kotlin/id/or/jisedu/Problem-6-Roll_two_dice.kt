package id.or.jisedu

import java.util.*

fun main(args: Array<String>) {
  print("Please enter the sum (between 2 and 12): ")
  val sc = Scanner(System.`in`)
  val number =  (sc.nextLine().toInt())
  val r = Random()
  while (true) {
    val a = r.nextInt(11) + 1
    val b = r.nextInt(11) + 1
    println("$a and $b = ${a + b}")
    if(a+b == number) return
  }
}