package id.or.jisedu

import java.util.*


//A Hacker has managed to find out the four digits for an online PIN code,
// but doesn’t know the correct sequence needed to hack into the account.
// Design and write a program that displays all the possible combinations
// for any four numerical digits entered by the user.
// The program should avoid displaying the same combination more than once.

//Solution by Hamza ALI - 32350.
//Language: Kotlin
fun main(args: Array<String>) {
  println("Please provide the 4 digits:")
  val sc = Scanner(System.`in`)
  val withVals =
    generatePerm(mutableListOf(sc.nextLine().toInt(), sc.nextLine().toInt(), sc.nextLine().toInt(), sc.nextLine().toInt()))
  println(withVals.distinct().joinToString(", \n"))
  
}

private fun <E> generatePerm(original: MutableList<E>): List<List<E>> {
  if (original.size == 0) {
    val result = ArrayList<List<E>>()
    result.add(ArrayList())
    return result
  }
  val first = original.removeAt(0)
  val toReturn = ArrayList<List<E>>()
  val perms = generatePerm(original)
  for (tmpPerms in perms) {
    for (index in 0..tmpPerms.size) {
      val temp = ArrayList(tmpPerms)
      temp.add(index, first)
      toReturn.add(temp)
    }
  }
  return toReturn
}
