package id.or.jisedu

import java.math.BigInteger

fun main(args: Array<String>) {
  print("Enter Number: ")
  val n = try {
    BigInteger(readLine())
  } catch (e: Exception) {
    10.toBigInteger()
  }
  println("\nFibonacci '$n': ")
  
  // seed is 0 and 1
  fibonacci(BigInteger.ZERO, BigInteger.ONE, n)
  println("\n")
  
}

private tailrec fun fibonacci(a: BigInteger, b: BigInteger, n: BigInteger): BigInteger {
  print("$a, ")
  return if (a >= n) a else fibonacci(b, a + b, n - BigInteger.ONE)
}