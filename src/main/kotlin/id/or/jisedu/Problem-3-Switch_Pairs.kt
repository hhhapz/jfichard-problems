package id.or.jisedu

import java.util.*

fun main(args: Array<String>) {
  print("Please enter the word you would like to switch: ")
  val n = Scanner(System.`in`).nextLine()
  println(n switch_pairs 2)
}

infix fun String.switch_pairs(n: Int): String {
  val charset = toCharArray()
  for(i in 0 until charset.size - 1){
    if(i % 2 == 1) continue
    val tmp = charset[i]
    charset[i] = charset[i+1]
    charset[i+1] = tmp
  }
  return charset.joinToString("")
}