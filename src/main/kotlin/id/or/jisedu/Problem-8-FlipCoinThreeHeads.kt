package id.or.jisedu

import java.util.*

fun main(args: Array<String>) {
  var count = 0
  val flips = mutableListOf<Int>()
  while (count < 3) {
    val number = Random().nextInt(2)
    count += number
    if (number == 0) count = 0
    flips.add(number)
  }
  println(flips.joinToString(" ").replace("1", "H").replace("0", "T"))
  println("Three heads in a row!")
}