package id.or.jisedu

import java.util.*

fun main(args: Array<String>) {
  print("Please enter the triangle size: ")
  val sc = Scanner(System.`in`)
  triangle(sc.nextLine().toInt())
}

fun triangle(size: Int) {
  for (i in 1..size) {
    val spaces = Array(size - i) { " " }
    val stars = Array(i) { "*" }
    println(spaces.joinToString("") + stars.joinToString(""))
  }
}